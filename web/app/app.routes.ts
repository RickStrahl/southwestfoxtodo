import {Routes} from '@angular/router';
import {About} from './about/about';
import {Home} from './home/home';
import {Todo} from "./todo/todo";

export const rootRouterConfig: Routes = [
  {path: '', redirectTo: 'todo', pathMatch: 'full'},
  {path: 'home', component: Home},
  {path: 'about', component: About},
  {path: 'todo', component: Todo }
];

