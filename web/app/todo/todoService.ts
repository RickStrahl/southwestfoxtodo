import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {TodoItem} from "./todo";
import {Http} from "@angular/http";

@Injectable()
export class TodoService {

	baseUrl = "http://localhost/todo/";

    constructor(private http:Http) {
    }

    getTodos() {
	    return this.http.get(this.baseUrl + "todos.td")
		    .map( response => {
		    	// transform the response
			    // into an object
			    return response.json();
		    })
		    .catch( () => {
			    // rethrow the error as something easier to work
			    // with. Normally you'd return an error object
			    // with parsed error info
			    return Observable.throw("failed to get todo items");
		    });
    }

    addTodo(todo:TodoItem) {
    	return this.http.put(this.baseUrl + "todo.td",todo)
		    .map( response => {
		    	return true;
		    })
		    .catch( response => {
		    	return Observable.throw("Failed to save to do item: " + todo.title);
		    });
    }

    removeTodo(todo:TodoItem) {
	    return this.http.delete(this.baseUrl + "todo.td?title=" + todo.title)
		    .map( response => {
			    return true;
		    })
		    .catch(response => {
			    return Observable.throw( "Failed to save to do item: " + todo.title);
		    });

    }
}