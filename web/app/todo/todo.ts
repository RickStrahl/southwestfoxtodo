import { Component, OnInit } from '@angular/core';
import {TodoService} from "./todoService";

@Component({
    //moduleId: module.id,
    selector: 'todo',
	styleUrls: ['./todo.css'],
    templateUrl: './todo.html'
})
export class Todo implements OnInit {
	constructor(private service:TodoService) { }

	todos:TodoItem[]  = [
		{
			title: "SWFox Angular Presentation",
			description: "Try to show up on time this time",
			completed: false
		},
		{
			title: "Do FoxPro presentation",
			description: "Should go good, let's hope I don't fail...",
			completed: false
		},
		{
			title: "Do raffle at the end of day one",
			description: "Should go good, let's hope I don't fail...",
			completed: false
		},
		{
			title: "Arrive at conference",
			description: "Arrive in Phoenix and catch a cab to hotel",
			completed: true
		}
	];
	activeTodo:TodoItem = new TodoItem();
	formVisible = true;
	message = "";


    ngOnInit() {
    }

    toggleCompleted(todo:TodoItem) {
    	todo.completed = !todo.completed;
    }

	loadTodos() {
		this.service.getTodos()
			.subscribe( todos => {
				this.todos = todos;
			}, msg => {
				this.showError(msg);
			});
	}

    removeTodo(todo:TodoItem) {
	    this.service.removeTodo(todo)
		    .subscribe( result => {
				    this.todos = this.todos.filter((t, i) => t.title != todo.title)
			    }, msg => {
				    this.showError(msg);
			    });
    }

    addTodo(todo,form) {

	    this.service.addTodo(todo)
		    .subscribe((result)=> {
				    this.todos.unshift(todo);
				    this.activeTodo = new TodoItem();
				    form.reset();
			    }, msg => {
				    this.showError(msg);
			    });
    }



    showError(msg){
    	this.message = msg;
    }

}

export class TodoItem {
	title:string = null;
	description:string = null;
	completed:boolean = false;
}
