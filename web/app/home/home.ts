import {Component, OnInit, OnDestroy} from '@angular/core';
import {DatePipe} from "@angular/common";

@Component({
  selector: 'home',
  styleUrls: ['./home.css'],
  templateUrl: './home.html'
})
export class Home implements  OnInit, OnDestroy {
	name= "Bill";
	date = new Date();
	intervalHandle = null;

	constructor() {
		this.intervalHandle = setInterval(()=> {
			this.date = new Date();
		},3000);
	}
	ngOnInit() {

	}
	ngOnDestroy() {
		clearInterval(this.intervalHandle);
	}

	sayHello(name?:string) {
		if (!name)
			name = this.name;

		// date formatter - same as template uses
		let dp = new DatePipe('en-US');
		let timeString = dp.transform(this.date,'h:mm:ss a')

        // note this is a JavaScript interpolated string, not a template!
		return `Hello ${name}. Time is ${timeString}`;
	}

	clickHello(form1) {
		debugger;
		alert(this.sayHello(this.name));
	}
}
