import {NgModule} from '@angular/core'
import {RouterModule} from "@angular/router";
import {rootRouterConfig} from "./app.routes";
import {AppComponent} from "./app";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {
	HttpModule, Http, ConnectionBackend, Request, RequestOptionsArgs, Response,
	RequestOptions, XHRBackend, ResponseOptions, BrowserXhr, XSRFStrategy
} from "@angular/http";

import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import {About} from './about/about';
import {Home} from './home/home';
import {Todo} from "./todo/todo";
import {TodoService} from "./todo/todoService";

@NgModule({
  declarations: [AppComponent,
    About,
    Home,
    Todo
  ],
  imports     : [
	  BrowserModule,
	  FormsModule,
	  HttpModule,
	  RouterModule.forRoot(rootRouterConfig)],
  providers   : [
	TodoService,
  	{provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap   : [AppComponent]
})
export class AppModule {

}
