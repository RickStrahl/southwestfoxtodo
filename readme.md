﻿# West Wind Web Connection Todo Angular 2.0 Sample Application

Source code to the Todo Angular sample from the walk through in the [West Wind Web Connection](http://west-wind.com/webconnection/) documentation.


The walk through can be found here:

* [Step by Step: An introduction to Angular 2.0 with Web Connection](http://west-wind.com/webconnection/docs/_4q40neruw.htm)


Created for Southwest Fox 2016.