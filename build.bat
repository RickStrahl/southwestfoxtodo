echo off

REM Your Web Connection Install Path
set src="C:\WEBCONNECTION\FOX"

REM The Project Path
set tgt="c:\WebConnectionProjects\Todo"
set appname="Todo"

REM force to current path even when running as Admin
md %tgt%\build
cd %tgt%\build
del *.* /q

copy %src%\*.dll

copy %src%\%appname%.exe
copy %tgt%\deploy\%appname%.ini
copy %tgt%\deploy\config.fpw

REM add 7zip to your path or in this folder for this to work
7z a %appname%_Packaged.zip *.*

pause